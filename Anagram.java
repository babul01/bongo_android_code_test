import java.util.Scanner;
import java.util.Arrays;

public class Anagram{
	
	public static void main(String[] args){
		
		String str1, str2;
		Scanner in = new Scanner(System.in);

		System.out.print("Enter first string:");
		str1 = in.nextLine();

		System.out.print("Enter second string:");
		str2 = in.nextLine();

		if(areAnagram(str1, str2)){
			
			System.out.println(str1+" and "+str2+" are Anagrams");

		}else{
			
			System.out.println(str1+" and "+str2+" are not Anagrams");
		}
	}

	private static boolean areAnagram(String str1, String str2){
		
		int str1Length = str1.length();
		int str2Length = str2.length();

		if(str1Length != str2Length){
			
			return false;
		}

		char[] w1 = str1.replaceAll("[\\s]","").toLowerCase().toCharArray();
		char[] w2 = str2.replaceAll("[\\s]","").toLowerCase().toCharArray();
		Arrays.sort(w1);
		Arrays.sort(w2);

		return Arrays.equals(w1, w2);
	}
}
