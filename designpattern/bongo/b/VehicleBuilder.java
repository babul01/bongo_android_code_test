package designpattern.bongo.b;

public class VehicleBuilder implements Vehicle{
	
	private int num_of_wheels;
	private int num_of_passengers;
	private boolean has_gas;
	
	@Override
	public VehicleBuilder set_num_of_wheels(int num_of_wheels) {
		this.num_of_wheels = num_of_wheels;
		return this;
	}
	@Override
	public VehicleBuilder set_num_of_passengers(int num_of_passengers) {
		this.num_of_passengers = num_of_passengers;
		return this;
	}
	@Override
	public VehicleBuilder has_gas(boolean has_gas) {
		this.has_gas = has_gas;
		return this;
	}
	
	public Car getCar() {
		return new Car(num_of_wheels, num_of_passengers, has_gas);
	}
	
	public Plane getPlane() {
		return new Plane(num_of_wheels, num_of_passengers, has_gas);
	}
	
}
