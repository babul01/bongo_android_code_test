package designpattern.bongo.b;

public class Main {

	public static void main(String[] args) {
		
		VehicleBuilder vb = new VehicleBuilder();
		
		System.out.println("Car Info");
		Car car = vb.set_num_of_wheels(4).set_num_of_passengers(3).has_gas(true).getCar();
		System.out.println(car);
		
		System.out.println("\nPlane Info");
		Plane plane = vb.set_num_of_wheels(3).set_num_of_passengers(100).has_gas(true).getPlane();
		System.out.println(plane);
	}

}
