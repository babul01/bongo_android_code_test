package designpattern.bongo.b;

public class Car {
	
	private int num_of_wheels;
	private int num_of_passengers;
	private boolean has_gas;
	
	public Car(int num_of_wheels, int num_of_passengers, boolean has_gas) {

		this.num_of_wheels = num_of_wheels;
		this.num_of_passengers = num_of_passengers;
		this.has_gas = has_gas;
	}

	@Override
	public String toString() {
		return "Car [num_of_wheels=" + num_of_wheels + ", num_of_passengers=" + num_of_passengers + ", has_gas="
				+ has_gas + "]";
	}
	
}
