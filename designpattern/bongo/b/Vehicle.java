package designpattern.bongo.b;

public interface Vehicle {
	VehicleBuilder set_num_of_wheels(int num_of_wheels);
	VehicleBuilder set_num_of_passengers(int num_of_passengers);
	VehicleBuilder has_gas(boolean has_gas);
}
