package designpattern.bongo.a;

public class VehicleFactory {
	
	public Vehicle getInstance(String vehicleName) {
		return vehicleName.equals("Car") ? new Car() : new Plane();
	}
}
