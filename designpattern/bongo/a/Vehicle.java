package designpattern.bongo.a;

public interface Vehicle {
	int set_num_of_wheels(int num_of_wheels);
	int set_num_of_passengers(int num_of_passengers);
	boolean has_gas(boolean has_gas);
}
