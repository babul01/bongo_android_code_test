package designpattern.bongo.a;

public class Car implements Vehicle {
	
	@Override
	public int set_num_of_wheels(int num_of_wheels) {
		return num_of_wheels;
	}

	@Override
	public int set_num_of_passengers(int num_of_passengers) {
		return num_of_passengers;
	}

	@Override
	public boolean has_gas(boolean has_gas) {
		return has_gas;
	}
}
