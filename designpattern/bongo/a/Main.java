package designpattern.bongo.a;

public class Main {

	public static void main(String[] args) {
		
		VehicleFactory vf = new VehicleFactory();
		
		System.out.println("Car Info");
		Vehicle car = vf.getInstance("Car");
		System.out.println(car.set_num_of_wheels(4));
		System.out.println(car.set_num_of_passengers(3));
		System.out.println(car.has_gas(true));
		
		System.out.println("\nPlane Info");
		Vehicle plane = vf.getInstance("Plane");
		System.out.println(plane.set_num_of_wheels(3));
		System.out.println(plane.set_num_of_passengers(100));
		System.out.println(plane.has_gas(true));
	}

}
